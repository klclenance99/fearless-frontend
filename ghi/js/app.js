function createCard(name, description, pictureUrl, location, starts, ends) {
    return `
      <div class="col-6 col-md-4">
      <div class="shadow p-3 mb-5 bg-body rounded d-grid gap-3">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${starts}-${ends}</div>
      </div>
      </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
    
        if (!response.ok) {
          alert('bad response recieved')
        } else {
          const data = await response.json();
          let totalText = ''
          for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name
          const starts = details.conference.starts
          const ends = details.conference.ends
          const html = createCard(name, description, pictureUrl, location, starts, ends);
          totalText += html;
          }
          }
          const row = document.querySelector('.row');
          row.innerHTML = totalText;
        }
      } catch (e) {
        alert("an error has ocurred please try again")
      }
});
