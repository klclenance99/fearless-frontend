window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);
        if (!response.ok) {
            alert('bad response recieved')
        }
        else {
            const data = await response.json();
            const stateTag = document.querySelector('#state')
            for (let state of data.states) {
                const newOption = document.createElement("option");
                newOption.value = state.abbreviation
                newOption.innerHTML = state.name
                stateTag.appendChild(newOption)
            }
            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
            method: "post",
            body: json,
           headers: {
           'Content-Type': 'application/json',
           },
           };
           const response = await fetch(locationUrl, fetchConfig);
           if (response.ok) {
           formTag.reset();
           const newLocation = await response.json();
           console.log(newLocation);
           }
        })
    }
})