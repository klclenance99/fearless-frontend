window.addEventListener('DOMContentLoaded', async () => {
    const conferenceTag = document.querySelector('#conference')
    const conferenceUrl = 'http://localhost:8000/api/conferences/'
    const response = await fetch(conferenceUrl)
    if (!response.ok) {
       alert('bad response received') 
    }
    else {
        const data = await response.json()
        for (let conference of data.conferences) {
          const newOption = document.createElement('option');
          newOption.value =  conference.id;
          newOption.innerHTML = conference.name;
          conferenceTag.appendChild(newOption); 
        }
        const formTag = document.querySelector("#create-presentation-form");
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const conferenceID = conferenceTag.options[conferenceTag.selectedIndex].value
            const presentationUrl = `http://localhost:8000/api/conferences/${conferenceID}/presentations/`;
            const fetchConfig = {
                method: 'post',
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                    },
            };
            const response = await fetch (presentationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newPresentation = await response.json();
                console.log(newPresentation);
           }
            })
        }
    }
        )