import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            presenter_name: '',
            company_name: '',
            presenter_email: '',
            title: '',
            synopsis: '',
            conferences: []
          };
        this.handlePresenterNameChange = this.handlePresenterNameChange.bind(this);
        this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
        this.handlePresenterEmailChange = this.handlePresenterEmailChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
        this.handleConferenceChange = this.handleConferenceChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.conferences;
        console.log(data);

        const presentationUrl = `http://localhost:8000/api/conferences/${this.state.conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
       const response = await fetch(presentationUrl, fetchConfig);
       if (response.ok) {
        const newPresentation = await response.json();
        console.log(newPresentation);

        const cleared = {
            presenter_name: '',
            company_name: '',
            presenter_email: '',
            title: '',
            synopsis: '',
            conference: '',
          };
          this.setState(cleared);
       }
       }
    handlePresenterNameChange(event) {
        const value = event.target.value;
        this.setState({presenter_name: value})
      }
    handleCompanyNameChange(event) {
        const value = event.target.value;
        this.setState({company_name: value})
    }
    handlePresenterEmailChange(event) {
        const value = event.target.value;
        this.setState({presenter_email: value})
    }
    handleTitleChange(event) {
        const value = event.target.value;
        this.setState({title: value})
    }
    handleSynopsisChange(event) {
        const value = event.target.value;
        this.setState({synopsis: value})
    }
    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({conference: value})
    }
    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({conferences: data.conferences});
          
        }
    }
  render() {
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input placeholder="Presenter_name" onChange={this.handlePresenterNameChange} value={this.state.presenter_name} required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Presenter_email" onChange={this.handlePresenterEmailChange} value={this.state.presenter_email} required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Company_name" onChange={this.handleCompanyNameChange} value={this.state.company_name} type="text" id="company_name" name="company_name" className="form-control" />
                <label htmlFor="company_name">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Title" onChange={this.handleTitleChange} value={this.state.title} type="text" id="title" name="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea placeholder="Synopsis" onChange={this.handleSynopsisChange} value={this.state.synopsis} id="synopsis" name="synopsis" className="form-control" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select required id="conference" onChange={this.handleConferenceChange} value={this.state.conference} className="form-select" name="conference">
                  <option value="">Choose a Conference</option>
                  {this.state.conferences.map(conference => {
                   return (
                    <option key={conference.id} value={conference.id}>
                     {conference.name}
                    </option>
                          );
                      })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  };
}


export default PresentationForm;