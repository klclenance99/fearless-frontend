import React from 'react';
import Nav from './Nav';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import AttendeesList from './AttendeesList';
import PresentationForm from './PresentationForm';
import { BrowserRouter } from "react-router-dom";
import { Routes } from "react-router-dom";
import { Route } from "react-router-dom";



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
       <Nav />
      <div className="container">
     {/* <AttendeeForm /> <ConferenceForm /> <LocationForm /> <AttendeesList attendees={props.attendees} /> */}
          <Routes>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="attendees">
            <Route path="new" element={<AttendeeForm />} />
            </Route>
            <Route path="locations">
            <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
            </Route>
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}
  

export default App;
